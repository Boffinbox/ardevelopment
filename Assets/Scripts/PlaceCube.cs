﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Android;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;
using UnityEngine.UI;

public class PlaceCube : MonoBehaviour
{
    private ARRaycastManager arOrigin;
    Pose pose;
    bool validPoint;
    bool placementMode = false;
    [SerializeField] GameObject indicator;
    [SerializeField] GameObject model;
    [SerializeField] Text instructions;
    void Start()
    {
        arOrigin = FindObjectOfType<ARSessionOrigin>().GetComponent<ARRaycastManager>();
        indicator.GetComponentInChildren<MeshRenderer>().material.color = new Color(0.2f, 1.0f, 0.2f);
    }
    void Update()
    {
        PoseUpdater();
        UpdateIndicator();
        if (Input.GetTouch(0).phase == TouchPhase.Moved || Input.GetTouch(0).phase == TouchPhase.Stationary)
        {
            indicator.SetActive(true);
            model.SetActive(true);
            placementMode = true;
            UpdateModel();
        }
        else
        {
            indicator.SetActive(false);
            placementMode = false;
        }
    }
    private void PoseUpdater()
    {
        Vector2 center = Camera.main.ViewportToScreenPoint(new Vector2 (0.5f, 0.5f));
        List<ARRaycastHit> listOfValidPoints = new List<ARRaycastHit>();
        arOrigin.Raycast(center, listOfValidPoints, TrackableType.Planes);
        if (listOfValidPoints.Count > 0)
        {
            validPoint = true;
            instructions.text = "Valid Plane Detected";
        }
        else
        {
            validPoint = false;
            instructions.text = "No Valid Plane";
        }
        if (validPoint)
        {
            // we do this to get the closest valid point to the camera - wouldn't want to place objects under the table now, would we?
            pose = listOfValidPoints[0].pose;
        }
    }
    private void UpdateIndicator()
    {
        if (validPoint)
        {
            indicator.transform.SetPositionAndRotation(pose.position, pose.rotation);
        }
    }
    private void UpdateModel()
    {
        if (validPoint && indicator.activeInHierarchy)
        {
            model.transform.SetPositionAndRotation(pose.position, pose.rotation);
        }
    }
}
