﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class BeatPerMinuteCalculator : MonoBehaviour
{
    [SerializeField] GameObject cube;
    [SerializeField] RuntimeAnimatorController dancinController;
    void Start()
    {
        AudioProcessor audioProcessor = FindObjectOfType<AudioProcessor>();
        audioProcessor.onBeat.AddListener(beatDetected);
    }
    void beatDetected()
    {
        ChangeColour(cube);
        AddBeatToDetection();
        Debug.Log("Beat!!!");
    }
    private void ChangeColour(GameObject gameObject)
    {
        Color newColour = new Color(Random.Range(0.2f, 1f), Random.Range(0.2f, 1f), Random.Range(0.2f, 1f));
        gameObject.GetComponent<MeshRenderer>().material.color = newColour;
        Debug.Log(newColour.r.ToString() + ", " + newColour.g.ToString() + ", " + newColour.b.ToString() + ".");
    }
    private void AddBeatToDetection()
    {

    }
}